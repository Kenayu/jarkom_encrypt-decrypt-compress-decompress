# Jarkom_Encrypt Decrypt Compress Decompress

Name    : Niken Ayu Anggarini

NRP     : 4210181003

This is a File Transfer program which allows a client to send a file to server.
At first, the program will encrypt and compress the file, and then send it to the
server. After receiving the file, the server will extract and decrypt the file.