﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransferServer
{
    class Program
    {
        private static string directoryPath = @".\1";

        static void Main(string[] args)
        {
            DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);

            TcpListener tcpListener = new TcpListener(IPAddress.Any, 1234);
            tcpListener.Start();

            Console.WriteLine("Server dimulai");

            //Infinite loop untuk terkoneksi dengan client baru   
            while (true)
            {
                // Menerima TcpClient    
                TcpClient tcpClient = tcpListener.AcceptTcpClient();

                Console.WriteLine("Terhubung ke client");

                StreamReader reader = new StreamReader(tcpClient.GetStream());

                // 
                string cmdFileSize = reader.ReadLine();
                string cmdFileName = reader.ReadLine();

                // Menerima file dari client
                int length = Convert.ToInt32(cmdFileSize);
                byte[] buffer = new byte[length];
                int received = 0;
                int read = 0;
                int size = 1024;
                int remaining = 0;
 
                // Membaca byte dari client
                while (received < length)
                {
                    remaining = length - received;
                    if (remaining < size)
                    {
                        size = remaining;
                    }

                    read = tcpClient.GetStream().Read(buffer, received, size);
                    received += read;
                }

                // Menyimpan file menggunakan filename yang dikirim oleh client
                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
                {
                    fStream.Write(buffer, 0, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }

                Console.WriteLine("File diterima dan disimpan di " + Environment.CurrentDirectory);

                //Dekompresi file
                /* foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
                {
                    Decompress(fileToDecompress);
                }*/

                // Deskripsi file
                Console.WriteLine("Masukkan filepath dari file yang ingin di deskripsi.");
                string startFilePath = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Masukkan filepath tujuan.");
                string targetFilePath = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Masukkan password anda.");
                string secretKey = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Tekan enter untuk melanjutkan.");
                Console.ReadLine();

                string decryptedFilePath = Console.ReadLine();
            }
        }

        // Fungsi dekompresi / extract
        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine($"Dekompresi: {fileToDecompress.Name}");
                    }
                }
            }
        }

        // Fungsi deskripsi
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            File.WriteAllBytes(file, bytesDecrypted);
        }
    }
}
