﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

namespace FileTransferClient
{
    class Program
    {

        private static string directoryPath = @".\1";

        static void Main(string[] args)
        {
            try
            {
                // Enkripsi File
                Console.WriteLine("Masukkan filepath file yang akan dienkripsi.");
                string startFilePath = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Masukkan filepath tujuan setelah file dienkripsi.");
                string targetFilePath = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Masukkan password anda.");
                string secretKey = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Tekan enter untuk melanjutkan.");
                Console.ReadLine();

                EncryptFile(startFilePath, targetFilePath, secretKey);
                Console.WriteLine("File anda berhasil dienkripsi.");

                // Kompresi file
                DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);
                Compress(directorySelected); 

                string fileName = targetFilePath;

                // Membuat koneksi dengan server
                TcpClient tcpClient = new TcpClient("127.0.0.1", 1234);
                Console.WriteLine("Terhubung. Mengirimkan file ke server.");

                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());

                byte[] bytes = File.ReadAllBytes(fileName);

                sWriter.WriteLine(bytes.Length.ToString());
                sWriter.Flush();

                sWriter.WriteLine(fileName);
                sWriter.Flush();

                Console.WriteLine("Mengirim file");
                tcpClient.Client.SendFile(fileName);

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }

        // Fungsi untuk mengenkripsi file
        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }
            return encryptedBytes;
        }

        public static void EncryptFile(string file, string fileEncrypted, string password)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            File.WriteAllBytes(fileEncrypted, bytesEncrypted);
        }

        // Fungsi untuk meng-kompres file
        public static void Compress(DirectoryInfo directorySelected)
        {
            foreach (FileInfo fileToCompress in directorySelected.GetFiles())
            {
                using (FileStream originalFileStream = fileToCompress.OpenRead())
                {
                    if ((File.GetAttributes(fileToCompress.FullName) &
                       FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                    {
                        using (FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                        {
                            using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                               CompressionMode.Compress))
                            {
                                originalFileStream.CopyTo(compressionStream);

                            }
                        }
                        FileInfo info = new FileInfo(directoryPath + Path.DirectorySeparatorChar + fileToCompress.Name + ".gz");
                        Console.WriteLine($"Mengkompress {fileToCompress.Name} dari {fileToCompress.Length.ToString()} ke {info.Length.ToString()} bytes.");
                    }

                }
            }
        }
    }
}

